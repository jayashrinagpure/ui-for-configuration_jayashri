package com.bse.configurator.request.type;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestTypeRepository extends JpaRepository<RequestType, Long>{
	@Query("select s from RequestType s where s.requestType=?1 and s.requestMedia=?2")
	RequestType findBySourceType(String request_type,String request_media);

	@Query("select s from RequestType s where s.id=?1 and s.requestType=?2 and s.requestMedia=?3")
	RequestType findBySourceId(Long id,String request_type,String request_media);
}

