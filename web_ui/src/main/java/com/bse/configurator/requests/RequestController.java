package com.bse.configurator.requests;

import java.util.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.bse.configurator.company.CompanyEnglish;
import com.bse.configurator.company.CompanyEnglishController;
import com.bse.configurator.company.CompanyEnglishRepository;
import com.bse.configurator.company.hindi.CompanyHindi;
import com.bse.configurator.company.hindi.CompanyHindiController;
import com.bse.configurator.company.hindi.CompanyHindiRepository;
import com.bse.configurator.email.Email;
import com.bse.configurator.email.EmailService;
import com.bse.configurator.exception.ResourceNotFoundException;
import com.bse.configurator.keyword.Keyword;
import com.bse.configurator.keyword.KeywordController;
import com.bse.configurator.keyword.KeywordMedia;
import com.bse.configurator.keyword.KeywordRepository;
import com.bse.configurator.keyword.KeywordType;
import com.bse.configurator.request.type.RequestType;
import com.bse.configurator.request.type.RequestTypeRepository;
import com.bse.configurator.source.SourceRepository;
import com.bse.configurator.source.SourceType;
import com.bse.configurator.source.facebook.FacebookSource;
import com.bse.configurator.source.facebook.FacebookSourceController;
import com.bse.configurator.source.facebook.FacebookSourceRepository;
import com.bse.configurator.source.twitter.TwitterSource;
import com.bse.configurator.source.twitter.TwitterSourceController;
import com.bse.configurator.source.twitter.TwitterSourceRepository;
import com.bse.configurator.util.Constants;

import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("/request")
public class RequestController {
	private RequestRepository requestRepository;
	private TwitterSourceRepository twitterSourceRepository;
	private FacebookSourceRepository facebookSourceRepository;
	private KeywordRepository keywordRepository;
	private SourceRepository sourceRepository;
	private CompanyEnglishRepository companyEngRepository;
	private CompanyHindiRepository companyHindiRepository;

	private Email email;
	@Autowired
	EmailService emailService;

	@Autowired
	private TwitterSourceController twitterCntrlr;

	@Autowired
	private FacebookSourceController facebookCntrlr;

	@Autowired
	private KeywordController keywordCntrlr;

	@Autowired
	private CompanyEnglishController companyCntrlr;
	
	@Autowired
	private CompanyHindiController companyHindiCntrlr;

	@Autowired
	public RequestController(RequestRepository requestRepository, RequestTypeRepository requestTypeRepository,
			FacebookSourceRepository facebookSourceRepository, TwitterSourceRepository twitterSourceRepository,
			KeywordRepository keywordRepository, SourceRepository sourceRepository,
			CompanyEnglishRepository companyEngRepository,CompanyHindiRepository companyHindiRepository) {
		this.requestRepository = requestRepository;
		this.sourceRepository = sourceRepository;
		this.facebookSourceRepository = facebookSourceRepository;
		this.twitterSourceRepository = twitterSourceRepository;
		this.keywordRepository = keywordRepository;
		this.companyEngRepository = companyEngRepository;
		this.companyHindiRepository = companyHindiRepository;
		email = new Email();
	}

	private void addRequest(Request request, RequestStatus requestStatus, String username, String comment,Date actionedDate) {
		request.setRequestStatus(requestStatus);
		// request.setId(id);
		request.setActionedDate(actionedDate);
		request.setComment(comment);
		request.setActionedBy(username);
		this.requestRepository.save(request);
	}

	@GetMapping
	public List<Request> getRequest() {
		return this.requestRepository.findAllRequest();
		// return new ArrayList<Request>();
	}

	@GetMapping("/{requestStatus:pending|accepted|rejected}")
	public List<Request> getSourcesByStatus(@PathVariable("requestStatus") String requestStatus) {
		switch (requestStatus) {
		case "pending":
			return this.requestRepository.findRecordsByStatus(RequestStatus.PENDING);
		case "accepted":
			return this.requestRepository.findRecordsByStatus(RequestStatus.APPROVED);
		case "rejected":
			return this.requestRepository.findRecordsByStatus(RequestStatus.REJECTED);
		}
		return null;
	}

	@PutMapping("/pending/{id}")
	public void approvePending(@RequestBody Request request, @PathVariable("id") Long id) {
		boolean authorized = false;
		Date actionedDate = new Date();
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Request requestInfoById = this.requestRepository.findById(id);
		RequestType currentRequestType = request.getRequestType();
		if (authorized) {
			if (requestInfoById == null) {
				throw new ResourceNotFoundException((long) 400, "Request information not found.");
			} else {
				// find add/activate/deactivate
				if (requestInfoById.getRequestType().getRequestType().equals(Constants.RSS)) {// web source
				} else if (requestInfoById.getRequestType().getRequestMedia().equals(Constants.COMPANY_ENGLISH)) { // company
				} else if (requestInfoById.getRequestType().getRequestType().equals(Constants.SELECTION) || requestInfoById.getRequestType().getRequestType().equals(Constants.REJECTION)) {
					KeywordType keywordType = KeywordType.valueOf(currentRequestType.getRequestType().toUpperCase());
					KeywordMedia keywordMedia = KeywordMedia
							.valueOf(currentRequestType.getRequestMedia().toUpperCase());
					Keyword keyword = new Keyword(requestInfoById.getRequestedValue(), keywordType, keywordMedia, true);
					keyword.setRequestedBy(requestInfoById.getRequestedBy());
					keyword.setRequestId(id);
					keyword.setComment(request.getComment());
					keyword.setRequestedDate(requestInfoById.getRequestedDate());
					keyword.setApproveDate(actionedDate);
					keywordCntrlr.addKeyword(keyword, currentRequestType.getRequestMedia());
				
				}
				else if (requestInfoById.getRequestType().getRequestMedia().equals(Constants.TWITTER)){
					SourceType sourceType = twitterCntrlr.getSourceType(requestInfoById.getRequestType().getRequestType());
					String[] nameOfsource = requestInfoById.getRequestedValue().split("\\|");
					String receivedTwitterSourceName = nameOfsource[0];
					String twitterSourceName = receivedTwitterSourceName.trim();
					String receivedTwitterDisplayName = nameOfsource[1];
					TwitterSource twitterSource = new TwitterSource(twitterSourceName,receivedTwitterDisplayName,
							currentRequestType.getRequestType(), true);
					twitterSource.setRequestedBy(requestInfoById.getRequestedBy());
					twitterSource.setRequestId(id);
					twitterSource.setApproveDate(actionedDate);
					twitterSource.setComment(request.getComment());
					twitterSource.setRequestedDate(requestInfoById.getRequestedDate());
					twitterSource.setSourceType(sourceType);
					twitterCntrlr.addSource(twitterSource);
				} else if (requestInfoById.getRequestType().getRequestMedia().equals(Constants.FACEBOOK)) {// fb source
					SourceType sourceType = facebookCntrlr.getSourceType(requestInfoById.getRequestType().getRequestType());
					FacebookSource facebookSource = new FacebookSource(requestInfoById.getRequestedValue(),
							currentRequestType.getRequestType(), true);
					facebookSource.setRequestedBy(requestInfoById.getRequestedBy());
					facebookSource.setRequestId(id);
					facebookSource.setRequestedDate(requestInfoById.getRequestedDate());
					facebookSource.setApproveDate(actionedDate);
					facebookSource.setComment(request.getComment());
					facebookSource.setSourceType(sourceType);
					facebookCntrlr.addSource(facebookSource);
				}
				this.addRequest(requestInfoById, RequestStatus.APPROVED, username, request.getComment(),actionedDate);
				this.email.setMessage(String.format("%s has approved addition of '%s', %s %s at %s requested by %s.", requestInfoById.getActionedBy(),
						request.getRequestedValue(),currentRequestType.getRequestMedia(), currentRequestType.getRequestType() , actionedDate, requestInfoById.getRequestedBy()));
				this.emailService.send(this.email);
			}
		}
	}

	@PutMapping("/pending/rejected/{id}")
	public void rejectPending(@RequestBody Request request, @PathVariable("id") Long id) {
		boolean authorized = false;
		Date actionedDate = new Date();
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Request requestInfoById = this.requestRepository.findById(id);
		RequestType currentRequestType = request.getRequestType();
		if (authorized) {
			if (requestInfoById == null) {
				throw new ResourceNotFoundException((long) 400, "Request information not found.");
			} else {
				/*
				 * String oldDetails = requestInfoById.getExistingValue(); String requestInfo =
				 * requestInfoById.getRequestedValue(); String requestDetails = "ADDED: " +
				 * currentRequestType.getRequestType() + " |" +
				 * currentRequestType.getRequestMedia(); Request updatedRequest = new
				 * Request("rejected", currentRequestType, requestDetails, oldDetails,
				 * requestInfoById.getRequestedBy());
				 */
				this.addRequest(requestInfoById, RequestStatus.REJECTED, username, request.getComment(),actionedDate);
				this.email.setMessage(String.format("%s has rejected %s of '%s', %s,%s at %s requested by %s.", requestInfoById.getActionedBy(),requestInfoById.getRequestedOperation().toString().toLowerCase(),
						request.getRequestedValue(),currentRequestType.getRequestMedia(), currentRequestType.getRequestType() , actionedDate, requestInfoById.getRequestedBy()));
				this.emailService.send(this.email);
			}
		}
	}

	@PutMapping("/pending/active/{id}")
	public void activatePending(@RequestBody Request request, @PathVariable("id") Long id) {
		boolean authorized = false;
		Date actionedDate = new Date();
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Request requestInfoById = this.requestRepository.findById(id);
		RequestType currentRequestType = request.getRequestType();
		if (authorized) {
			if (requestInfoById == null) {
				throw new ResourceNotFoundException((long) 400, "Request information not found.");
			} else {
				if (requestInfoById.getRequestType().getRequestType().equals(Constants.RSS)) {// web for now nothing as data comes from feedly
				} else if (requestInfoById.getRequestType().getRequestMedia().equals(Constants.COMPANY_ENGLISH)) { // company
					
				} else if(requestInfoById.getRequestType().getRequestType().equals(Constants.SELECTION) || requestInfoById.getRequestType().getRequestType().equals(Constants.REJECTION)){// keyword
					KeywordMedia keywordMedia = KeywordMedia
							.valueOf(currentRequestType.getRequestMedia().toUpperCase());
					KeywordType keywordType = KeywordType.valueOf(currentRequestType.getRequestType().toUpperCase());
					Keyword existingKeyword = this.keywordRepository.doesKeywordExists(requestInfoById.getRequestedValue(),
							keywordMedia);
					Keyword newKeyword = new Keyword(existingKeyword.getKeyword(), existingKeyword.getType(),
							keywordMedia, true);
					newKeyword.setRequestedBy(requestInfoById.getRequestedBy());
					newKeyword.setRequestId(id);
					newKeyword.setType(keywordType);
					newKeyword.setComment(request.getComment());
					newKeyword.setApproveDate(actionedDate);
					newKeyword.setRequestedDate(requestInfoById.getRequestedDate());
					keywordCntrlr.approveKeywordRequest(existingKeyword, newKeyword, username,requestInfoById.getRequestedBy());
				}else if (requestInfoById.getRequestType().getRequestMedia().equals(Constants.TWITTER)) {// tw source
					SourceType sourceType = this.sourceRepository.findBySourceType(currentRequestType.getRequestType(),
							currentRequestType.getRequestMedia());
					String[] nameOfsource = requestInfoById.getRequestedValue().split("\\|");
					String receivedTwitterSourceName = nameOfsource[0];
					String twitterSourceName = receivedTwitterSourceName.trim();
					String receivedTwitterDisplayName = nameOfsource[1];
					TwitterSource existingSource = this.twitterSourceRepository.doesSourceExists(sourceType,
							twitterSourceName);
					existingSource.setSourceTypeName(currentRequestType.getRequestType());
					TwitterSource newTwitterSource = new TwitterSource(twitterSourceName,receivedTwitterDisplayName,existingSource.getSourceTypeName(), true);
					newTwitterSource.setSourceTypeName(currentRequestType.getRequestType());
					newTwitterSource.setRequestedBy(requestInfoById.getRequestedBy());
					newTwitterSource.setRequestId(id);
					newTwitterSource.setSourceType(sourceType);
					newTwitterSource.setComment(request.getComment());
					newTwitterSource.setApproveDate(actionedDate);
					newTwitterSource.setRequestedDate(requestInfoById.getRequestedDate());
					twitterCntrlr.approveTwitterSourceRequest(existingSource, newTwitterSource, username,requestInfoById.getRequestedBy());
				} else if (requestInfoById.getRequestType().getRequestMedia().equals(Constants.FACEBOOK)) {// fb source
					SourceType sourceType = this.sourceRepository.findBySourceType(currentRequestType.getRequestType(),
							currentRequestType.getRequestMedia());
					FacebookSource existingSource = this.facebookSourceRepository.doesSourceExists(sourceType,
							requestInfoById.getRequestedValue());
					existingSource.setSourceTypeName(currentRequestType.getRequestType());
					FacebookSource newFbSource = new FacebookSource(existingSource.getSourceName(),existingSource.getSourceTypeName(), true);
					newFbSource.setSourceTypeName(currentRequestType.getRequestType());
					newFbSource.setRequestedBy(requestInfoById.getRequestedBy());
					newFbSource.setRequestId(id);
					newFbSource.setSourceType(sourceType);
					newFbSource.setComment(request.getComment());
					newFbSource.setApproveDate(actionedDate);
					newFbSource.setRequestedDate(requestInfoById.getRequestedDate());
					facebookCntrlr.approveFacebookSourceRequest(existingSource, newFbSource, username,requestInfoById.getRequestedBy());
				}
				this.addRequest(requestInfoById, RequestStatus.APPROVED, username, request.getComment(),actionedDate);
				this.email.setMessage(String.format("%s has approved activation of '%s' %s, %s at %s requested by %s", requestInfoById.getActionedBy(),
						requestInfoById.getRequestedValue(),currentRequestType.getRequestType(), currentRequestType.getRequestMedia(),actionedDate,
						requestInfoById.getRequestedBy()));
				this.emailService.send(this.email);
			}
		}
	}

	@PutMapping("/pending/inactive/{id}")
	public void deactivatePending(@RequestBody Request request, @PathVariable("id") Long id) {
		boolean authorized = false;
		Date actionedDate = new Date();
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Request requestInfoById = this.requestRepository.findById(id);
		RequestType currentRequestType = request.getRequestType();
		if (authorized) {
			if (requestInfoById == null) {
				throw new ResourceNotFoundException((long) 400, "Request information not found.");
			} else {
				if (requestInfoById.getRequestType().getRequestType().equals(Constants.RSS)) {// web source
				} else if (requestInfoById.getRequestType().getRequestMedia().equals(Constants.COMPANY_ENGLISH)) { // company
					// web for now nothing as data comes from feedly
				}  else if(requestInfoById.getRequestType().getRequestType().equals(Constants.SELECTION) || requestInfoById.getRequestType().getRequestType().equals(Constants.REJECTION)){// keyword
					KeywordMedia keywordMedia = KeywordMedia
							.valueOf(currentRequestType.getRequestMedia().toUpperCase());
					System.out.println("currentRequestType media "+currentRequestType.getRequestMedia()+" id "+currentRequestType.getId());
					KeywordType keywordType = KeywordType.valueOf(currentRequestType.getRequestType().toUpperCase());
					Keyword existingKeyword = this.keywordRepository.doesKeywordExists(requestInfoById.getRequestedValue(),
							keywordMedia);

					System.out.println("existingKeyword media "+existingKeyword.getMedia()+" id "+existingKeyword.getId());
					Keyword newKeyword = new Keyword(existingKeyword.getKeyword(), existingKeyword.getType(),
							keywordMedia, false);

					System.out.println("newKeyword media "+newKeyword.getMedia()+" id "+newKeyword.getId());
					newKeyword.setRequestedBy(requestInfoById.getRequestedBy());
					newKeyword.setRequestId(id);
					newKeyword.setType(keywordType);
					newKeyword.setComment(request.getComment());
					newKeyword.setRequestedDate(requestInfoById.getRequestedDate());
					newKeyword.setApproveDate(actionedDate);
					keywordCntrlr.approveKeywordRequest(existingKeyword, newKeyword, username,requestInfoById.getRequestedBy());
				}else if (requestInfoById.getRequestType().getRequestMedia().equals(Constants.TWITTER)) {// tw source
					SourceType sourceType = this.sourceRepository.findBySourceType(currentRequestType.getRequestType(),
							currentRequestType.getRequestMedia());
					String[] nameOfsource = requestInfoById.getRequestedValue().split("\\|");
					String receivedTwitterSourceName = nameOfsource[0];
					String twitterSourceName = receivedTwitterSourceName.trim();
					String receivedTwitterDisplayName = nameOfsource[1];
					TwitterSource existingSource = this.twitterSourceRepository.doesSourceExists(sourceType,
							twitterSourceName);
					existingSource.setSourceTypeName(currentRequestType.getRequestType());
					TwitterSource newTwitterSource = new TwitterSource(twitterSourceName,receivedTwitterDisplayName,existingSource.getSourceTypeName(), false);
					newTwitterSource.setSourceTypeName(currentRequestType.getRequestType());
					newTwitterSource.setRequestedBy(requestInfoById.getRequestedBy());
					newTwitterSource.setRequestId(id);
					newTwitterSource.setSourceType(sourceType);
					newTwitterSource.setComment(request.getComment());
					newTwitterSource.setRequestedDate(requestInfoById.getRequestedDate());
					newTwitterSource.setApproveDate(actionedDate);
					twitterCntrlr.approveTwitterSourceRequest(existingSource, newTwitterSource, username,requestInfoById.getRequestedBy());
				} else if (requestInfoById.getRequestType().getRequestMedia().equals(Constants.FACEBOOK)) {// fb source
					SourceType sourceType = this.sourceRepository.findBySourceType(currentRequestType.getRequestType(),
							currentRequestType.getRequestMedia());
					FacebookSource existingSource = this.facebookSourceRepository.doesSourceExists(sourceType,
							requestInfoById.getRequestedValue());
					existingSource.setSourceTypeName(currentRequestType.getRequestType());
					FacebookSource newFbSource = new FacebookSource(existingSource.getSourceName(),existingSource.getSourceTypeName(), false);
					newFbSource.setSourceTypeName(currentRequestType.getRequestType());
					newFbSource.setRequestedBy(requestInfoById.getRequestedBy());
					newFbSource.setRequestId(id);
					newFbSource.setSourceType(sourceType);
					newFbSource.setComment(request.getComment());
					newFbSource.setApproveDate(actionedDate);
					newFbSource.setRequestedDate(requestInfoById.getRequestedDate());
					facebookCntrlr.approveFacebookSourceRequest(existingSource, newFbSource, username,requestInfoById.getRequestedBy());
				}
				this.addRequest(requestInfoById, RequestStatus.APPROVED, username, request.getComment(),actionedDate);
				this.email.setMessage(String.format("%s has approved deactivation of '%s' %s, %s at %s requested by %s", requestInfoById.getActionedBy(),
						requestInfoById.getRequestedValue(),currentRequestType.getRequestType(), currentRequestType.getRequestMedia(),actionedDate,
						requestInfoById.getRequestedBy()));
				this.emailService.send(this.email);
			}
		}
	}

	@PutMapping("/pending/company/english/{id}")
	public void updatePendingGramEnglish(@RequestBody Request request, @PathVariable("id") Long id) {
		boolean authorized = false;
		Date actionedDate = new Date();
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		// RequestType currentRequestType
		// =this.getRequestType(request.getRequestType().getId());
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Request existingCompanyRequest = this.requestRepository.findById(id);
		String companyGrams = request.getRequestedValue().split(":")[1];
		if (existingCompanyRequest == null) {
			throw new ResourceNotFoundException((long) 400, "Company details not found.");
		} else {
			// id = existingCompanyRequest.
			String receivedCompanyId = existingCompanyRequest.getRequestedValue().split(":")[0];
			long companyId = Long.parseLong(receivedCompanyId.trim());
			CompanyEnglish existingCompanyInfo = companyEngRepository.findById(companyId);
			if (authorized) {
				existingCompanyInfo.setEndDate(new Date());
				this.companyEngRepository.save(existingCompanyInfo);
				CompanyEnglish updatedCompany = new CompanyEnglish(existingCompanyInfo, true);
				updatedCompany.setRequestId(id);
				updatedCompany.setRequestedBy(existingCompanyRequest.getRequestedBy());
				updatedCompany.setRequestedDate(existingCompanyRequest.getRequestedDate());
				updatedCompany.setComment(request.getComment());
				updatedCompany.setGrams(companyGrams);
				updatedCompany.setApproveDate(actionedDate);
				companyCntrlr.approveCompanyRequest(existingCompanyInfo, updatedCompany, username,existingCompanyRequest.getRequestedBy());
				this.addRequest(existingCompanyRequest, RequestStatus.APPROVED, username, request.getComment(),actionedDate);
				this.email.setMessage(String.format("%s has approved %s company gram as '%s' at %s requested by %s.", updatedCompany.getApprovedBy(),
						updatedCompany.getName(), updatedCompany.getGrams(), updatedCompany.getStartDate(),updatedCompany.getRequestedBy()));
				this.emailService.send(this.email);
			}
		}
	}
	
	@PutMapping("/pending/company/hindi/{id}")
	public void updatePendingGramHindi(@RequestBody Request request, @PathVariable("id") Long id) {
		boolean authorized = false;
		Date actionedDate = new Date();
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Request existingCompanyRequest = this.requestRepository.findById(id);
		String companyGrams = request.getRequestedValue().split(":")[1];
		if (existingCompanyRequest == null) {
			throw new ResourceNotFoundException((long) 400, "Company details not found.");
		} else {
			// id = existingCompanyRequest.
			String receivedCompanyId = existingCompanyRequest.getRequestedValue().split(":")[0];
			long companyId = Long.parseLong(receivedCompanyId.trim());
			CompanyHindi existingCompanyInfo = companyHindiRepository.findById(companyId);
			if (authorized) {
				existingCompanyInfo.setEndDate(new Date());
				this.companyHindiRepository.save(existingCompanyInfo);
				CompanyHindi updatedCompany = new CompanyHindi(existingCompanyInfo, true);
				updatedCompany.setRequestId(id);
				updatedCompany.setRequestedBy(existingCompanyRequest.getRequestedBy());
				updatedCompany.setRequestedDate(existingCompanyRequest.getRequestedDate());
				updatedCompany.setComment(request.getComment());
				updatedCompany.setGrams(companyGrams);
				updatedCompany.setApproveDate(actionedDate);
				companyHindiCntrlr.approveCompanyRequest(existingCompanyInfo, updatedCompany, username,existingCompanyRequest.getRequestedBy());
				this.addRequest(existingCompanyRequest, RequestStatus.APPROVED, username, request.getComment(),actionedDate);
				this.email.setMessage(String.format("%s has approved %s company gram as '%s' at %s requested by %s.", updatedCompany.getApprovedBy(),
						updatedCompany.getName(), updatedCompany.getGrams(), updatedCompany.getStartDate(),updatedCompany.getRequestedBy()));
				this.emailService.send(this.email);
			}
		}
	}
}
