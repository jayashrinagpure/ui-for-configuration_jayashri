package com.bse.configurator.requests;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.bse.configurator.request.type.RequestType;

@Entity
@Table(name = "request")
public class Request {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	@Column(name = "request_id")
	private long id;
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "request_type_id", nullable = false)
	private RequestType requestType;

	@Column(name = "actioned_by")
	private String actionedBy;
	@Column(name = "comment")
	private String comment;

	@Column(name = "requested_by")
	private String requestedBy;

	private Date requestedDate = null;
	private Date actionedDate = null;

	@Enumerated(EnumType.STRING)
	@Column(name = "request_status")
	private RequestStatus requestStatus;

	@Column(name = "requested_value")
	private String requestedValue;

	@Column(name = "existing_value")
	private String existingValue;

	@Enumerated(EnumType.STRING)
	@Column(name = "requested_operation")
	private RequestOperation requestedOperation;

	public Request() {

	}
	
	public Request( RequestType requestType, String requestedValue,String existingValue, String requestedBy,String actionedBy,Date actionedDate,
			Date requestedDate,RequestOperation requestOperation) {
		this.requestStatus = RequestStatus.APPROVED;
		this.requestType = requestType;
		this.requestedValue = requestedValue;
		this.existingValue = existingValue;
		this.requestedBy = requestedBy;
		this.actionedBy = actionedBy;
		this.actionedDate = actionedDate;
		this.requestedDate = requestedDate;
		this.requestedOperation=requestOperation;
	}

	public Request( RequestType requestType, String requestedValue, String requestedBy,
			Date requestedDate,RequestOperation requestOperation) {
		this.requestStatus = RequestStatus.PENDING;
		this.requestType = requestType;
		this.requestedValue = requestedValue;
		this.existingValue = requestedValue;
		this.requestedBy = requestedBy;
		this.requestedDate = requestedDate;
		this.requestedOperation=requestOperation;
	}
	
	public Request( RequestType requestType, String requestedValue,String existingValue, String requestedBy,
			Date requestedDate,RequestOperation requestOperation) {
		this.requestStatus = RequestStatus.PENDING;
		this.requestType = requestType;
		this.requestedValue = requestedValue;
		this.existingValue = existingValue;
		this.requestedBy = requestedBy;
		this.requestedDate = requestedDate;
		this.requestedOperation=requestOperation;
	}

	public Request(RequestStatus requestStatus, RequestType requestType, String requestedValue, String existingValue,
			String requestedBy) {
		this();
		this.requestStatus = requestStatus;
		this.requestType = requestType;
		this.requestedValue = requestedValue;
		this.existingValue= existingValue;
		this.requestedBy = requestedBy;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	public Date getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(RequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}

	public RequestType getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}

	public String getRequestedValue() {
		return requestedValue;
	}

	public void setRequestedValue(String requestedValue) {
		this.requestedValue = requestedValue;
	}

	public String getExistingValue() {
		return existingValue;
	}

	public void setExistingValue(String existingValue) {
		this.existingValue = existingValue;
	}

	public RequestOperation getRequestedOperation() {
		return requestedOperation;
	}

	public void setRequestedOperation(RequestOperation requestedOperation) {
		this.requestedOperation = requestedOperation;
	}

	public Date getActionedDate() {
		return actionedDate;
	}

	public void setActionedDate(Date actionedDate) {
		this.actionedDate = actionedDate;
	}

	public String getActionedBy() {
		return actionedBy;
	}

	public void setActionedBy(String actionedBy) {
		this.actionedBy = actionedBy;
	}
}
