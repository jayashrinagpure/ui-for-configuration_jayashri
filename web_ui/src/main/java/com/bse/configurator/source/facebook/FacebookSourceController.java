package com.bse.configurator.source.facebook;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bse.configurator.email.Email;
import com.bse.configurator.email.EmailService;
import com.bse.configurator.exception.DuplicateFoundException;
import com.bse.configurator.exception.ResourceNotFoundException;
import com.bse.configurator.request.type.RequestType;
import com.bse.configurator.request.type.RequestTypeRepository;
import com.bse.configurator.requests.Request;
import com.bse.configurator.requests.RequestOperation;
import com.bse.configurator.requests.RequestRepository;
import com.bse.configurator.source.SourceRepository;
import com.bse.configurator.source.SourceType;
import com.bse.configurator.util.Constants;

import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("/source/facebook")
public class FacebookSourceController {

	@Autowired
	EmailService emailService;

	private FacebookSourceRepository facebookSourceRepository;
	private SourceType groupSource, pageSource, userSource;
	private RequestRepository requestRepository;
	private RequestTypeRepository requestTypeRepository;
	private Email email;

	@Autowired
	public FacebookSourceController(FacebookSourceRepository facebookSourceRepository,
			SourceRepository sourceRepository, RequestRepository requestRepository,
			RequestTypeRepository requestTypeRepository) {
		this.facebookSourceRepository = facebookSourceRepository;
		this.requestRepository = requestRepository;
		this.requestTypeRepository = requestTypeRepository;
		this.userSource = sourceRepository.findBySourceType("user", "facebook");
		this.pageSource = sourceRepository.findBySourceType("page", "facebook");
		this.groupSource = sourceRepository.findBySourceType("group", "facebook");
		email = new Email();
	}

	public SourceType getSourceType(String sourceTypeName) {
		SourceType sourceType = null;
		switch (sourceTypeName) {
		case "user":
			sourceType = this.userSource;
			break;
		case "page":
			sourceType = this.pageSource;
			break;
		case "group":
			sourceType = this.groupSource;
			break;
		}
		return sourceType;
	}

	public void saveFacebookSource(FacebookSource source, String approvedBy, String createdBy) {
		source.setApprovedBy(approvedBy);
		source.setRequestedBy(createdBy);
		facebookSourceRepository.save(source);
	}

	public void saveFacebookSource(FacebookSource source, String createdBy) {
		source.setApprovedBy(createdBy);
		source.setRequestedBy(createdBy);
		facebookSourceRepository.save(source);
	}

	private void addFacebookSource(FacebookSource source, SourceType currentSourceType, String username) {
		source.setApprovedBy(username);
		if (source.getRequestedBy() == null) {
			source.setRequestedBy(username);
		}
		if (source.getRequestedDate() == null) {
			source.setRequestedDate(new Date());
			source.setApproveDate(source.getStartDate());
		} else {
			source.setRequestedDate(source.getRequestedDate());
		}
		source.setSourceType(currentSourceType);
		this.facebookSourceRepository.save(source);
	}

	public void approveFacebookSourceRequest(FacebookSource existingSource, FacebookSource newSource, String approvedBy,
			String createdBy) {
		if (existingSource == null) {
			throw new ResourceNotFoundException((long) 400, "Keyword not found.");
		} else {
			existingSource.setEndDate(new Date());
			this.facebookSourceRepository.save(existingSource);
			this.saveFacebookSource(newSource, approvedBy, createdBy);
			this.emailService.send(this.email);
		}
	}

	@PostMapping
	public void addSource(@Valid @RequestBody FacebookSource source) {
		boolean authorized = false;
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		SourceType currentSourceType = this.getSourceType(source.getSourceTypeName());
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (authorized) {
			if (this.facebookSourceRepository.doesSourceExists(currentSourceType, source.getSourceName()) != null) {
				throw new DuplicateFoundException((long) 400, "Duplicate entry found for facebook source name.");
			} else {
				this.addFacebookSource(source, currentSourceType, username);

				if (source.getRequestedBy().equals(username)) {
					RequestType requestType = this.requestTypeRepository
							.findBySourceType(currentSourceType.getSourceType(), currentSourceType.getSourceMedia());
					Request request = new Request(requestType, source.getSourceName(), source.getSourceName(), username,
							username, new Date(), new Date(), RequestOperation.ADD);
					this.requestRepository.save(request);
				}
				this.email.setMessage(String.format("%s has added '%s' facebook source at %s.", username,
						source.getSourceName(), source.getStartDate()));
				this.emailService.send(this.email);
			}
		} else {
			if (this.facebookSourceRepository.doesSourceExists(currentSourceType, source.getSourceName()) != null) {
				throw new DuplicateFoundException((long) 400, "Duplicate entry found for facebook source name.");
			} else {
				String requestedValue = source.getSourceName();
				RequestType requestType = this.requestTypeRepository.findBySourceType(currentSourceType.getSourceType(),
						currentSourceType.getSourceMedia());
				Date requestedDate = new Date();
				Request request = new Request(requestType, requestedValue, username, requestedDate,
						RequestOperation.ADD);// for other user
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s has requested to add '%s' facebook source at %s.", username,
						source.getSourceName(), source.getStartDate()));
				this.emailService.send(this.email);
			}
		}
	}

	@PutMapping("/inactive/{id}")
	public void deActivateSource(@RequestBody FacebookSource source, @PathVariable("id") Long id) {
		boolean authorized = false;
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		SourceType currentSourceType = this.getSourceType(source.getSourceTypeName());
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		FacebookSource facebookSource = this.facebookSourceRepository.findByActiveSource(id, currentSourceType,
				source.getSourceName());
		if (authorized) {
			if (facebookSource == null) {
				throw new ResourceNotFoundException((long) 400, "Facebook source not found.");
			} else {
				facebookSource.setSourceTypeName(source.getSourceTypeName());
				facebookSource.setEndDate(new Date());
				this.facebookSourceRepository.save(facebookSource);
				FacebookSource updatedFacebookSource = new FacebookSource(facebookSource.getSourceName(),
						facebookSource.getSourceTypeName(), false);
				this.addFacebookSource(updatedFacebookSource, currentSourceType, username);
				RequestType requestType = this.requestTypeRepository.findBySourceType(currentSourceType.getSourceType(),
						currentSourceType.getSourceMedia());
				Request request = new Request(requestType, source.getSourceName(), source.getSourceName(), username,
						username, new Date(), new Date(), RequestOperation.INACTIVE);
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s has deactivated '%s' facebook source at %s.", username,
						source.getSourceName(), source.getStartDate()));
				this.emailService.send(this.email);
			}
		} else {
			if (facebookSource == null) {
				throw new ResourceNotFoundException((long) 400, "Facebook source not found.");
			} else {
				String requestedValue = source.getSourceName();
				RequestType requestType = this.requestTypeRepository.findBySourceType(currentSourceType.getSourceType(),
						currentSourceType.getSourceMedia());
				Date requestedDate = new Date();
				Request request = new Request(requestType, requestedValue, username, requestedDate,
						RequestOperation.INACTIVE);
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s has requested to deactivate '%s' facebook source at %s.", username,
						source.getSourceName(), source.getStartDate()));
				this.emailService.send(this.email);
			}
		}
	}

	@PutMapping("/active/{id}")
	public void activateSource(@RequestBody FacebookSource source, @PathVariable("id") Long id) {
		boolean authorized = false;
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		SourceType currentSourceType = this.getSourceType(source.getSourceTypeName());
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		FacebookSource facebookSource = this.facebookSourceRepository.findByInactiveSource(id, currentSourceType,
				source.getSourceName());
		if (authorized) {
			if (facebookSource == null) {
				throw new ResourceNotFoundException((long) 400, "Facebook source not found.");
			} else {
				facebookSource.setSourceTypeName(source.getSourceTypeName());
				facebookSource.setEndDate(new Date());
				this.facebookSourceRepository.save(facebookSource);
				FacebookSource updatedFacebookSource = new FacebookSource(facebookSource.getSourceName(),
						facebookSource.getSourceTypeName(), true);
				this.addFacebookSource(updatedFacebookSource, currentSourceType, username);
				RequestType requestType = this.requestTypeRepository.findBySourceType(currentSourceType.getSourceType(),
						currentSourceType.getSourceMedia());
				Request request = new Request(requestType, source.getSourceName(), source.getSourceName(), username,
						username, new Date(), new Date(), RequestOperation.ACTIVE);
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s has activated '%s' facebook source at %s.", username,
						source.getSourceName(), source.getStartDate()));
				this.emailService.send(this.email);
			}
		} else {// for other user
			if (facebookSource == null) {
				throw new ResourceNotFoundException((long) 400, "Facebook source not found.");
			} else {
				String requestedValue = source.getSourceName();
				;
				RequestType requestType = this.requestTypeRepository.findBySourceType(currentSourceType.getSourceType(),
						currentSourceType.getSourceMedia());
				Date requestedDate = new Date();
				Request request = new Request(requestType, requestedValue, username, requestedDate,
						RequestOperation.ACTIVE);
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s has requested to activate '%s' facebook source at %s.", username,
						source.getSourceName(), source.getStartDate()));
				this.emailService.send(this.email);
			}
		}
	}

	@GetMapping
	public List<FacebookSource> getSources() {
		return this.facebookSourceRepository.findAllRecords();
	}

	@GetMapping("/{status:active|inactive}")
	public List<FacebookSource> getSourcesByStatus(@PathVariable("status") String status) {
		switch (status) {
		case "active":
			return this.facebookSourceRepository.findRecordsByStatus(true);
		case "inactive":
			return this.facebookSourceRepository.findRecordsByStatus(false);
		}
		return null;
	}
}
