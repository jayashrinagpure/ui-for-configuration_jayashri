package com.bse.configurator.source.facebook.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FacebookSourceTypeValidator implements ConstraintValidator<FacebookSourceType, String> {

	@Override
	public void initialize(FacebookSourceType constraintAnnotation) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isValid(String facebookSourceTypeName, ConstraintValidatorContext context) {
		if (facebookSourceTypeName == null) {
			return false;
		} else if (facebookSourceTypeName.equals("user") || facebookSourceTypeName.equals("page")
				|| facebookSourceTypeName.equals("group"))
			return true;
		else
			return false;
	}

}
