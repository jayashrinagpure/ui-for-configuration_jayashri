package com.bse.configurator.source.web;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface WebSourceEnglishRepository  extends JpaRepository<WebSourceEnglish, Long>{
	@Query("select t from WebSourceEnglish t where t.endDate='9999-12-31 23:59:59'")
	List<WebSourceEnglish> findAllRecords();
}
