package com.bse.configurator.source.web;

import java.util.List;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bse.configurator.source.SourceType;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("/source/web/english")
public class WebSourceEnglishController {
	private WebSourceEnglishRepository webSourceEnglishRepository;
	private SourceType sourceType;
	public WebSourceEnglishController(WebSourceEnglishRepository webSourceEnglishRepository) {
		this.webSourceEnglishRepository = webSourceEnglishRepository;
		this.sourceType= new SourceType("rss","web");
	
	}

	@PostMapping
	public void addSource(@RequestBody WebSourceEnglish source) {
		String userName = (String) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		source.setCreatedBy(userName);
		source.setSourceType(this.sourceType);
		this.webSourceEnglishRepository.save(source);
	}

	@GetMapping
	public List<WebSourceEnglish> getSources() {
		return this.webSourceEnglishRepository.findAll();
	}
}
