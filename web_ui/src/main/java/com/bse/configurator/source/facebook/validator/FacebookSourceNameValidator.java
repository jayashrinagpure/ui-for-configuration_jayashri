package com.bse.configurator.source.facebook.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.bse.configurator.source.facebook.FacebookSource;

public class FacebookSourceNameValidator implements ConstraintValidator<FacebookSourceName, FacebookSource> {

	@Override
	public void initialize(FacebookSourceName constraintAnnotation) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isValid(FacebookSource facebookSource, ConstraintValidatorContext context) {
		Pattern pattern = Pattern.compile("([A-Za-z0-9-_&/() \\[.*?\\]]+)");
		if (facebookSource.getSourceTypeName() == null) {
			return false;
		} else if (pattern.matcher(facebookSource.getSourceName()).matches() == true) {
			return true;
		} else {
			return false;
		}
	}

}
