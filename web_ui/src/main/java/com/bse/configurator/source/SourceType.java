package com.bse.configurator.source;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.bse.configurator.source.twitter.TwitterSource;
import com.bse.configurator.source.web.WebSourceEnglish;
import com.bse.configurator.source.web.hindi.WebSourceHindi;
import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(name="source_type")
public class SourceType {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private long id;
	private String sourceType,sourceMedia;
	@OneToMany(targetEntity=WebSourceEnglish.class,mappedBy="sourceType", cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	private List<WebSourceEnglish> webSourcesEnglish;
	@OneToMany(targetEntity=WebSourceHindi.class,mappedBy="sourceType", cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	private List<WebSourceHindi> webSourcesHindi;
	@OneToMany(targetEntity=TwitterSource.class,mappedBy="sourceType", cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<TwitterSource> twitterSources;

	public SourceType(){
		
	}
	
	public SourceType(String sourceType,String sourceMedia) {
		this.sourceType=sourceType;
		this.sourceMedia=sourceMedia;
	}
	public SourceType(long id,String sourceType,String sourceMedia) {
		this.id=id;
		this.sourceType=sourceType;
		this.sourceMedia=sourceMedia;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSourceType() {
		return sourceType;
	}
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	public String getSourceMedia() {
		return sourceMedia;
	}
	public void setSourceMedia(String sourceMedia) {
		this.sourceMedia = sourceMedia;
	}
	@JsonIgnore
	public List<WebSourceEnglish> getWebSourceEnglish(){
		return this.webSourcesEnglish;
	}
	@JsonIgnore
	public List<WebSourceHindi> getWebSourceHindi(){
		return this.webSourcesHindi;
	}
	
	@JsonIgnore
	public List<TwitterSource> getTwitterSources() {
		return twitterSources;
	}
	
}
