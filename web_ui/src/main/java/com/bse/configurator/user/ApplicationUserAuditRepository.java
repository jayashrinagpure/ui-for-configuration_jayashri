package com.bse.configurator.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface ApplicationUserAuditRepository extends JpaRepository<ApplicationUserAudit, Long> {
	ApplicationUserAudit findByUsername(String username);

}
