package com.bse.configurator.util;

public class Constants {
	
	/** The Constant BLANK. */
	public static final String BLANK = "";
	
	/** The Constant COLON. */
	public static final String COLON = " : ";
	
	/** The Constant DASH. */
	public static final String DASH = " - ";
	
	/** The Constant KEY_TXNID. */
	public static final String KEY_TXNID = "TXNID";
	
	/** The Constant XPATH_TXNID. */
	public static final String XPATH_TXNID = "//transcationId";
	
	/** The Constant ENTRY. */
	public static final String ENTRY = "Entry";
	
	/** The Constant EXIT. */
	public static final String EXIT = "Exit";
	
	public static final String OTHERUSER = "USER";
	
	public static final String SUPERUSER = "SUPER USER";
	
	public static final String GRAM = "company_gram";
	
	public static final String ENGLISH = "ENGLISH";
	
	public static final String HINDI = "HINDI";
	
	public static final String TWITTER = "twitter";
	
	public static final String FACEBOOK = "facebook";
	
	public static final String SELECTION = "selection";
	
	public static final String REJECTION = "rejection";
	
	public static final String RSS = "rss";
	
	public static final String COMPANY_ENGLISH = "all_english";
	
	public static final String COMPANY_HINDI = "all_hindi";
}