package com.bse.configurator.keyword;

import java.beans.Transient;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "keywords")
public class Keyword {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "keyword_id")
	private long id;

	
	@org.springframework.data.annotation.Transient
	private Boolean requested;

	@Column(name = "keyword")
	@NotNull
	@NotBlank
	private String keyword;

	@Enumerated(EnumType.STRING)
	@Column(name = "keyword_type")
	private KeywordType type;

	@Column(name = "keyword_status")
	private Boolean status = true;

	@Enumerated(EnumType.STRING)
	@Column(name = "keyword_media")
	private KeywordMedia media;


	private Date startDate = new Date();
	private Date endDate = new Date();
	private Date requestedDate =null;
	private Date approveDate = null;
	
	@Column(name="approved_by")
	private String approvedBy;
	
	@Column(name="requested_by")
	private String requestedBy;
	
	@Column(name="request_id")
	private long requestId;
	
	@Column(name="comment")
	private String comment;
	
	

	public Keyword() {
		try {
			this.endDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("9999-12-31 23:59:59");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public Keyword(String keyword,KeywordType keywordType,KeywordMedia keywordMedia,Boolean status) {
		this();
		this.keyword=keyword;
		this.type=keywordType;
		this.media=keywordMedia;
		this.status=status;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public KeywordType getType() {
		return type;
	}

	public void setType(KeywordType type) {
		this.type = type;
	}

	public KeywordMedia getMedia() {
		return media;
	}

	public void setMedia(KeywordMedia media) {
		this.media = media;
	}

	public String getKeyword() {
		return keyword.toLowerCase();
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword.toLowerCase();
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	public long getRequestId() {
		return requestId;
	}

	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Boolean getRequested() {
		return requested;
	}

	public void setRequested(Boolean requested) {
		this.requested = requested;
	}

}
