/**
 * @author Pushpa Gudmalwar
 * @name webApp.controller: logoutController
 * @description
 * #logoutController
 * It is logout controller.
 * This controller is responsible for loging out user and redirect to login page.
 * @requires $window
 * @requires $state
 */

app.controller('logoutController',function($window,$state){
	$window.localStorage.clear();
	$state.go("login");
});