/**
 * @author Jayashri Nagpure
 * @name webApp.controller: loginController
 * @description
 * #loginController
 * It is login controller.
 * This service is responsible for providing authentication to user.
 * @requires $scope
 * @requires $rootScope
 * @requires $state
 * @requires $timeout
 * @requires AuthenticationService
 * @requires authService
 * 
 * @property {object} user : object stores user.
 * @property {object} errors : object stores all errors.
 * @property {boolean} rememberMe : boolean Controls the boolean flag for remembering user. Initialized with true.
 * @property {boolean} permissions : boolean controls the boolean flag for permission to user. Initialized with false.
 */
app.controller('loginController', function ($rootScope, $scope, $state, $timeout,AuthenticationService,authService ) {
	$scope.user = {};
    $scope.errors = {};
    $scope.rememberMe = true;
    $rootScope.permissions = false;
    /**
     * Timed out function
     * @description - Angular timeout function.
     */
    $timeout(function () { 
       angular.element('[ng-model="username"]').focus();
   });
   /**
    * Login 
    * @description - Login function to authenticate and login sucessfully.
    * @param {object} credentials - User credntials. 
    */
    $scope.login = function (credentials) {
        event.preventDefault();
        AuthenticationService.Login(credentials).then(function (response) {
        	if(credentials.username == 'admin'){
        	}
			authService.loginConfirmed();
        	$rootScope.permissions = true;
             $state.transitionTo('homeParent.home'); 
        },
        function (error) {
            $rootScope.authenticationError = true;
            console.log($rootScope.authenticationError);
            authService.loginCancelled();
        });
    };

    /** 
     * Resets credentials
     * @description - Sets credentials to null.
    */
    $scope.reset = function(){
        $scope.credentials = null;
    }
});
