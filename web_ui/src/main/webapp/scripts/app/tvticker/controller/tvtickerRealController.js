/**
 * @author Lekha
 * @name webApp.controller: tvtickerRealController
 * @description
 * #tvtickerRealController
 * It is a video to text controller.
 * This controller is responsible for showing the Real time details of TV tickers listing top 10 tickers
 * @requires $state
 * @requires $rootScope
 * @requires $scope
 * @requires $http
 */
	app.controller("tvtickerRealController",function($rootScope,$scope,$http,$q,$state){
	$scope.menu.current="realtime";
	$scope.rowLimitOptionArray = [10,20,30,50,100];  
	$scope.selected = {rowLimit:$scope.rowLimitOptionArray[1]}
    $scope.NewsChannel = ['CNBC TV 18','Bloomberg Quint'];
    $scope.selectedNewsChannel =$scope.NewsChannel[1];
    $('#selectChannelModal').modal('show');
   // $("#selectChannelModal").modal({backdrop: false});
    
    /**
     * 
     */
	$scope.reset = function () {
		$scope.selectedNewsChannel = null;
		$scope.confirmSubmit = null;
	}
	
	/**
	 * 
	 */
	$scope.handleClick = function(datalist){
		$scope.realtimedatalist = datalist;
		$scope.selectedtimestamp = datalist.timestamps[0];
		$scope.selectedimage ="http://13.233.79.216/"+ datalist.links[0];
		$scope.content = datalist.content;
	};
	
	/**
	 * 
	 */
	$scope.handleRealtimeClick = function(selectedtimestamp){
		console.log('handle',selectedtimestamp);
	    var index =$scope.realtimedatalist.timestamps.indexOf(selectedtimestamp);
	    console.log('index',index);
	    $scope.selectedimage = "http://13.233.79.216/"+$scope.realtimedatalist.links[index];
	};
	
	/**
	 * Choose channel and send for processing
	 */
	$scope.chooseChannel = function(selectedNewsChannel){
		 websocket = new WebSocket("ws://13.233.79.216:5005/");
		 websocket.onmessage = function (event) { 
		 $scope.$apply(function(){
			$scope.realtimeTickerLists = JSON.parse(event.data);
		 });
         };
         websocket.onopen = function(event) {websocket.send(selectedNewsChannel);};
         websocket.onerror = function(event) {console.log("on error",event);};
         $('#selectChannelModal').modal('hide');
	};
});