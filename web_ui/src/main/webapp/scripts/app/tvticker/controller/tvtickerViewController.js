/**
 * @author Lekha
 * @name webApp.controller: videototextController
 * @description
 * #videototextController
 * It is a video to text controller.
 * This controller is responsible for showing the details video to text converison.
 * @requires $state
 * @requires $rootScope
 * @requires $scope
 * @requires $http
 */
app.controller('tvtickerViewController', function($rootScope,$scope) {
	$scope.menu.current="tvticker";
	$scope.selectedtimestamp = $rootScope.viewData.timestamps[0];
	$scope.selectedimage = "http://13.233.79.216/"+$rootScope.viewData.links[0];
	
	$scope.handleClick = function(selectedtimestamp) {
	    var index = $rootScope.viewData.timestamps.indexOf(selectedtimestamp);
	    $scope.selectedimage = "http://13.233.79.216/"+$rootScope.viewData.links[index];
	};	
});