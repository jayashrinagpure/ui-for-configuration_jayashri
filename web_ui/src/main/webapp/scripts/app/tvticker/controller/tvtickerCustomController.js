/**
 * @author Jayashri Nagpure
 * @name webApp.controller: tvtickerTestController
 * @description
 * #tvtickerTestController
 * It is a video to text controller.
 * This controller is responsible for showing the details video to text conversion.
 * @requires $state
 * @requires $rootScope
 * @requires $scope
 * @requires $http
 */
	app.controller("tvtickerCustomController",function($rootScope,$scope, $http, $q, tvTickerService,$filter){
	$scope.menu.current="custom";
	$scope.rowLimitOptionArray = [10,20,30,50,100];  
	$scope.selected = {rowLimit:$scope.rowLimitOptionArray[1]}
	var deferred = $q.defer();
	$scope.date = new Date();
    $scope.NewsChannel = ['Bloomberg Quint','CNBC TV 18'];
    $scope.selectedNewsChannel =$scope.NewsChannel[0];

	$scope.startDate = $filter('date')(new Date(), 'EEE-yyyy-MM-dd');
	$scope.endDate = $filter('date')(new Date(), 'EEE-yyyy-MM-dd');

	$scope.handleClick = function(selectedNewsChannel) {
	    getCustomTickerList(selectedNewsChannel);
	}

	getCustomTickerList = function(selectedNewsChannel){
		tvTickerService.getCustomData(selectedNewsChannel)
		 .then(function(response) {
			 try{
				 $scope.customTickerLists = JSON.parse(response.data);
			 } catch(e){
				 $scope.customTickerLists =response.data;
			 }
		 },
		 function(reason) {
		 });
	}
	
	/**
	 * 
	 */
	$scope.getViewData = function(viewdata){
		$rootScope.viewData = viewdata;
	};
	
	getCustomTickerList($scope.selectedNewsChannel);
	  
});