app.controller("hindi_CompanyController", function ($rootScope, $scope, $http, $q, Flash, $filter,exportToExcelService,sortDataService) {

	// to heighlight company menu setting current menu as company
	$scope.menu.current = "hindi_company";

	//sorting column 
	$scope.sortColumn = "name";
	$scope.reverseSort = false;

	//page range selection options	
	$scope.rowLimitOptionArray = [10, 20, 30, 50, 100];
	$scope.selected = {
		rowLimit: $scope.rowLimitOptionArray[1]
	}

	// for defered promise approach
	var deferred = $q.defer();

	/**
	 * Get list of company 
	 */
	getCompanyList = function () {
		$http({
				method: "GET",
				url: 'company/hindi'
			})
			.then(function (response) {
				response.data.forEach(function (_company) {
					_company.isEdit = false; // to hide or show edit icon
					_company.companyGram = _company.grams;
				});
				$scope.companyLists = response.data;
				$scope.exportInfo = angular.copy(response.data);
				deferred.resolve(response);
			}, function (reason) {
				$scope.error = reason.data;
				deferred.reject(err);
			});
		return deferred.promise;
	}

	getCompanyList(); //On load  

	/**
	 * Set company grams
	 * _company: contains only single company details 
	 * Oldgram: previous company grams
	 */
	$scope.editGram = function (_company, Oldgram) {
		var newGram;
		// if edit icon clicked
		$scope.companyLists.forEach(function (x) {
			if (x.id == _company.id) {
				x.isEdit = true;
				newGram = x.grams;
			} else
				x.isEdit = false;
		})
		Oldgram.replace(/,\s*$/, "");
		_company.companyGram = newGram + ','; //adding comma after company grams
	};

	/**
	 * Saves company grams value into database
	 * _company:contains current company details
	 */
	$scope.doneEditing = function (_company) {
		_company.isEdit = false;
		_gram = _company.companyGram.replace(/,\s*$/, "");
		var companydata = {
			"grams": _gram.toLowerCase()
		};
		$http({
			method: "PUT",
			url: 'company/hindi/' + _company.id,
			data: companydata
		}).then(function (response) {
			getCompanyList();
			if ($rootScope.userRole == 'super_user') {
				var message = '<strong>' + _gram.toUpperCase() + '</strong> edited successfully';
				var id = Flash.create('success', message);
			} else if ($rootScope.userRole == 'user') {
				var message = '<strong>' + _gram.toUpperCase() + '</strong> is pending for approval  .';
				var id = Flash.create('success', message);
			}
		}, function (reason) {
			$scope.error = reason.data;
			var message = '<strong>' + _gram.toUpperCase() + '</strong> unable to edit';
			var id = Flash.create('danger', message);
		});
	};

    /**
     * Canceles operation of editing company gram
     * _company:contains current company details
     */
	$scope.cancel = function (_company) {
		_company.isEdit = false;
	};

	//sorting column data after click on column name
	$scope.sortData = function (column) {
		sortDataService.sortData(column,$scope.sortColumn,$scope.reverseSort);
		$scope.reverseSort = sortDataService.sortDataServiceObject.reverseSort;
		$scope.sortColumn = sortDataService.sortDataServiceObject.sortColumn;
	}

	//getting column name to sort data
	$scope.getSortClass = function (column) {
		return sortDataService.getSortClass(column,$scope.sortColumn,$scope.reverseSort);
	}

    /**
     * Exports to Excel
     */
    $scope.export = function(){
    	var table = $scope.exportInfo;
    	 table = $filter('orderBy')(table, 'name');
    	var csvString = '<table><tr><td>Company Full Name</td><td>Company Abbreviated Name</td><td>Scrip Code</td><td>Scrip Id</td><td>Company Gram(s)</td></tr>';
    	for(var i=0; i<table.length;i++){
    		var rowData = table[i];
    		csvString = csvString + "<tr><td>" +rowData.name + "</td><td>" + rowData.abbrName+"</td><td>" 
    		+ rowData.scripCode+"</td><td>" + rowData.scripId+"</td><td>" + rowData.companyGram+"</td>";
    		csvString = csvString + "</tr>";
	    }
    	csvString += "</table>";
     	csvString = csvString.substring(0, csvString.length);
     	exportToExcelService.converttoExcel(csvString,'Company');	        
     }
    
    /**
     * Exports to pdf
     */
    $scope.exportpdf = function () {
    	{
    		var item = $scope.exportInfo;
    		item = $filter('orderBy')(item, 'name');
    	    var doc = new jsPDF();   
    	    var col = ["Company Full Name","Company Abbreviated Name","Scrip Code","Scrip Id","Company Gram(s)"];
    	    var rows = [];
    	    var dateInfo,statusInfo;	    	    
    	    for(var i=0; i<item.length;i++){
        		var rowData = item[i];
        		dateInfo=new Date(rowData.startDate);
        		var temp = [rowData.name, rowData.abbrName, rowData.scripCode,rowData.scripId,rowData.companyGram];
        		rows.push(temp);
    	    }
    	   doc.autoTable(col, rows);
    	   doc.save('Company'+ new Date().toDateString() +'.pdf');
    	  }
    };
});