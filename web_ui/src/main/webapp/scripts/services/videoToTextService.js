/**
 * @author Jayashri Nagpure
 * @name webApp.service: videoToTextService
 * @description
 * #
 * 
 * .
 */

app.service('videoToTextService', function ($http,$q) {
	/**
	 * @description:To get video list of suspicious videos
	 */
    this.getVideoList = function() {
        var deferred = $q.defer();
        $http({
                method: 'POST',
                url: 'http://13.233.79.216:5004/video_list'
            })
            .then(function(data){
                deferred.resolve(data);
            },function(data){
            	 errorToasty("Failed to load data");
                 deferred.reject(data)
            });
        return deferred.promise;
    };
    /**
     * @description:To get rumours video metadata
     */
    this.getRumoursMetadata = function(_date) {
    	console.log('_date',_date)
        var deferred = $q.defer();
        $http({
                method: 'POST',
                url: 'http://13.233.79.216:5004/video_metadata/'+_date
            })
            .then(function(data){
                deferred.resolve(data);
            },function(data){
            	 errorToasty("Failed to load data");
                 deferred.reject(data)
            });
        return deferred.promise;
    };
    /**
     * @description:To get next video chunk with metadata
     */
    this.getMetadata = function() {
        var deferred = $q.defer();
        $http({
                method: 'POST',
                url: 'http://13.233.79.216:5004/get_chunk'
            })
            .then(function(data){
                deferred.resolve(data);
            },function(data){
            	 errorToasty("Failed to load data");
                 deferred.reject(data)
            });
        return deferred.promise;
    };


});