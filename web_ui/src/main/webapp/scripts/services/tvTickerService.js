/**
 * @author Jayashri Nagpure
 * @name webApp.service: tvTickerService
 * @description
 * #
 * 
 * .
 */

app.service('tvTickerService', function ($http,$q) {
	/**
	 * @description:To get tvticker custom data
	 */
    this.getCustomData = function(selectedNewsChannel) {
        var deferred = $q.defer();
        var formdata = new FormData();
        formdata.append('source_name', selectedNewsChannel);
        $http.post(
        		'http://13.233.79.216:5007/custom', formdata, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }
            )
            .then(
                function(data) {
                 deferred.resolve(data)
                },
                function(data) {
               	 errorToasty("Failed to load data");
                 deferred.reject(data)
                }
            );
        return deferred.promise;        
    };
	/**
	 * @description: To get tvticker realtime data
	 */
    this.getRealTimeData = function(selectedNewsChannel) {
        var deferred = $q.defer();
        var formdata = new FormData();
        formdata.append('source_name', selectedNewsChannel);
        $http.post(
        		'http://13.233.79.216:5007/realtime', formdata, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }
            )
            .then(
                function(data) {
                 deferred.resolve(data)
                },
                function(data) {
               	 errorToasty("Failed to load data");
                 deferred.reject(data)
                }
            );
        return deferred.promise;        
    };
});