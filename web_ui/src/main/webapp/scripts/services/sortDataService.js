app.service('sortDataService', function () {	
	 this.sortDataServiceObject = {};
	 this.sortData = function(column,sortColumn,reverseSort){
		 this.sortDataServiceObject.reverseSort = (sortColumn == column) ? !reverseSort : false;
		 this.sortDataServiceObject.sortColumn = column;
		 return this.sortDataServiceObject;
	 }
	 
	 this.getSortClass = function(column,sortColumn,reverseSort){
			if (sortColumn == column) {
				return reverseSort ? 'arrow-down' : 'arrow-up';
			}
	 }

});